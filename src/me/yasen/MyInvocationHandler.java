package me.yasen;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by ygeorgiev on 10/27/16.
 */
public class MyInvocationHandler implements InvocationHandler {

    private Object object;

    public MyInvocationHandler(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(object, args);
        } catch (ClassCastException e) {
            System.out.println("asd");

            throw e;
        }

    }
}
