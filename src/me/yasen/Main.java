package me.yasen;

import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        Interface1 obj = new Child();
//        Interface1 obj = (Interface1) ProxyFactory.getInstance(new Child(), new Class[] { Interface1.class });

        testMethod(obj);
    }

    public static void testMethod(Interface1 obj) {
        ((Interface2) obj).methodB();
    }
}
