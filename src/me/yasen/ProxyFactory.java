package me.yasen;

import java.lang.reflect.Proxy;

/**
 * Created by ygeorgiev on 10/27/16.
 */
public class ProxyFactory {
    public static Object getInstance(Object object, Class[] interfaces) {
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), interfaces, new MyInvocationHandler(object));

    }
}
