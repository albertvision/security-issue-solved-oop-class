package me.yasen;

/**
 * Created by ygeorgiev on 10/27/16.
 */
public class Child implements Interface1, Interface2 {
    @Override
    public void methodA() {
        System.out.println("A called");
    }

    @Override
    public void methodB() {
        System.out.println("B called");
    }
}
